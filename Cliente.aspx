﻿<%@ Page Title="Cliente" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="Cliente.aspx.cs" Inherits="Cliente" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        .hero-wrap {
            background-image: url('https://ame.gob.ec/wp-content/uploads/2020/11/Boletin-UTR-3.jpeg');
            background-size: cover;
            background-position: center center;
            height: 500px; /* Ajusta la altura según tus necesidades */
        }
    </style>

    <section class="hero-wrap hero-wrap-2 js-fullheight">
        <div class="overlay"></div>
        <div class="container">
            <div class="row no-gutters slider-text js-fullheight align-items-end justify-content-center">
                <div class="col-md-9 ftco-animate pb-5 text-center">
                    <p class="breadcrumbs"><span class="mr-2"><a runat="server" href="~/">Home <i class="fa fa-chevron-right"></i></a></span> <span>Client <i class="fa fa-chevron-right"></i></span></p>
                    <h1 class="mb-0 bread">Cliente</h1>
                </div>
            </div>
        </div>
    </section>

    <div class="container mt-4">
        <h4>Client List</h4>
        <div class="row mt-3">
            <div class="col-md-12">
                <button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#addClientModal">
                    Add New Client
                </button>
            </div>
        </div>
        <br />
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <asp:GridView ID="gvClients" runat="server" CssClass="table table-striped table-bordered"
                        DataKeyNames="ClienteID" OnRowCommand="gvClients_RowCommand" OnRowEditing="gvClients_RowEditing"
                        OnRowDeleting="gvClients_RowDeleting">
                        <Columns>
                            
                        </Columns>
                    </asp:GridView>
                </div>
                <asp:HiddenField ID="hfEditClientID" runat="server" />
                <asp:HiddenField ID="hfDeleteClientID" runat="server" />
            </div>
        </div>
    </div>

    <!-- Add Client Modal -->
    <div class="modal fade" id="addClientModal" tabindex="-1" role="dialog" aria-labelledby="addClientModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="addClientModalLabel">Add New Client</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="txtName">Name:</label>
                        <asp:TextBox ID="txtName" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label for="txtApellido">Apellido:</label>
                        <asp:TextBox ID="txtApellido" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label for="txtEmail">Email:</label>
                        <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label for="txtPhoneNumber">Phone Number:</label>
                        <asp:TextBox ID="txtPhoneNumber" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label for="txtDireccion">Direccion:</label>
                        <asp:TextBox ID="txtDireccion" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <asp:Button ID="btnSaveClient" runat="server" Text="Save Client" OnClick="btnSaveClient_Click"
                        CssClass="btn btn-primary" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
