﻿<%@ Page Title="About" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="Destinos.aspx.cs" Inherits="Destinos" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <section class="hero-wrap hero-wrap-2 js-fullheight" style="background-image: url('https://multipasajes.travel/wp-content/uploads/2019/09/banos.jpg');">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text js-fullheight align-items-end justify-content-center">
          <div class="col-md-9 ftco-animate pb-5 text-center">
           <p class="breadcrumbs"><span class="mr-2"><a runat="server" href="~/">Home <i class="fa fa-chevron-right"></i></a></span> <span>Destinos <i class="fa fa-chevron-right"></i></span></p>
           <h1 class="mb-0 bread">Destinos</h1>
         </div>
       </div>
     </div>
    </section>
    <h2>Destinos</h2>

    <div class="container mt-4">
        <h4>Destino List</h4>
        <div class="row mt-3">
            <div class="col-md-12">
                <button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#addDestinoModal">
                    Add New Destino
                </button>
            </div>
        </div>
        <br />
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <asp:GridView ID="gvDestinos" runat="server" CssClass="table table-striped table-bordered" DataKeyNames="DestinoID" OnRowCommand="gvDestinos_RowCommand" OnRowEditing="gvDestinos_RowEditing" OnRowDeleting="gvDestinos_RowDeleting">
                        
                    </asp:GridView>
                </div>
                <asp:HiddenField ID="hfEditDestinoID" runat="server" />
                <asp:HiddenField ID="hfDeleteDestinoID" runat="server" />
            </div>
        </div>
    </div>

    <!-- Add Destino Modal -->
    <div class="modal fade" id="addDestinoModal" tabindex="-1" role="dialog" aria-labelledby="addDestinoModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="addDestinoModalLabel">Add New Destino</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="txtNombre">Nombre:</label>
                        <asp:TextBox ID="txtNombre" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label for="txtDescripcion">Descripción:</label>
                        <asp:TextBox ID="txtDescripcion" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label for="txtPrecio">Precio:</label>
                        <asp:TextBox ID="txtPrecio" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label for="txtImagenURL">Imagen URL:</label>
                        <asp:TextBox ID="txtImagenURL" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <asp:Button ID="btnSaveDestino" runat="server" Text="Save" CssClass="btn btn-primary" OnClick="btnSaveDestino_Click" />
                </div>
            </div>
        </div>
    </div>

    <!-- Edit Destino Modal -->
    <div class="modal fade" id="editDestinoModal" tabindex="-1" role="dialog" aria-labelledby="editDestinoModalLabel" aria-hidden="true">
        <!-- Modal content goes here -->
    </div>

    <!-- Delete Destino Modal -->
    <div class="modal fade" id="deleteDestinoModal" tabindex="-1" role="dialog" aria-labelledby="deleteDestinoModalLabel" aria-hidden="true">
        <!-- Modal content goes here -->
    </div>

    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
</asp:Content>
