﻿<%@ Page Title="Nosotros" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="About.aspx.cs" Inherits="About" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    
 <section class="hero-wrap hero-wrap-2 js-fullheight" style="background-image: url('https://lacasadelvolcanecuador.com/wp-content/uploads/2019/08/VOLCANTUNGUR-copia.jpg');">
  <div class="overlay"></div>
  <div class="container">
    <div class="row no-gutters slider-text js-fullheight align-items-end justify-content-center">
      <div class="col-md-9 ftco-animate pb-5 text-center">
       <p class="breadcrumbs"><span class="mr-2"><a href="index.html">Home <i class="fa fa-chevron-right"></i></a></span> <span>Nosotros<i class="fa fa-chevron-right"></i></span></p>
       <h1 class="mb-0 bread">Nosotros</h1>
     </div>
   </div>
 </div>
</section>

<br />


<section class="ftco-section ftco-about img"style="background-image: url(images/ecuador.png);">
 <div class="overlay"></div>
 <div class="container py-md-5">
  <div class="row py-md-5">
   <div class="col-md d-flex align-items-center justify-content-center">
    <a href="https://www.youtube.com/watch?v=wSLqcDL2R7Q" class="icon-video popup-vimeo d-flex align-items-center justify-content-center mb-4">
     <span class="fa fa-play"></span>
   </a>
 </div>
</div>
</div>
</section>

<section class="ftco-section ftco-about ftco-no-pt img">
 <div class="container">
  <div class="row d-flex">
   <div class="col-md-12 about-intro">
    <div class="row">
     <div class="col-md-6 d-flex align-items-stretch">
      <div class="img d-flex w-100 align-items-center justify-content-center" style="background-image:url(images/about-1.jpg);">
      </div>
    </div>
    <div class="col-md-6 pl-md-5 py-5">
      <div class="row justify-content-start pb-3">
        <div class="col-md-12 heading-section ftco-animate">
         <span class="subheading">Amazonia</span>
         <h2 class="mb-4">Un poquito mas alla de Baños</h2>
         <p>Cuando se conectan dos regiones, la sierra y el oriente, la naturaleza danza en armonía y la cultura se entrelaza, tejiendo un lienzo de maravillas que enamora los corazones</p>
         <p><a href="https://www.tourscostaecuador.com/toursoriente-amazoniadeecuador.php" class="btn btn-primary">Aqui para tours del Oriente</a></p>
       </div>
     </div>
   </div>
 </div>
</div>
</div>
</div>
</section>

<section class="ftco-section testimony-section bg-bottom" style="background-image: url(images/bg_1.jpg);">
 <div class="overlay"></div>
 <div class="container">
  <div class="row justify-content-center pb-4">
    <div class="col-md-7 text-center heading-section heading-section-white ftco-animate">
     <span class="subheading">Testimonial</span>
     <h2 class="mb-4">Tourist Feedback</h2>
   </div>
 </div>
 <div class="row ftco-animate">
  <div class="col-md-12">
    <div class="carousel-testimony owl-carousel">
      <div class="item">
        <div class="testimony-wrap py-4">
          <div class="text">
           <p class="star">
            <span class="fa fa-star"></span>
            <span class="fa fa-star"></span>
            <span class="fa fa-star"></span>
            <span class="fa fa-star"></span>
            <span class="fa fa-star"></span>
          </p>
          <p class="mb-4">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
          <div class="d-flex align-items-center">
           <div class="user-img" style="background-image: url(images/person_1.jpg)"></div>
           <div class="pl-3">
            <p class="name">Roger Scott</p>
            <span class="position">Marketing Manager</span>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="item">
    <div class="testimony-wrap py-4">
      <div class="text">
       <p class="star">
        <span class="fa fa-star"></span>
        <span class="fa fa-star"></span>
        <span class="fa fa-star"></span>
        <span class="fa fa-star"></span>
        <span class="fa fa-star"></span>
      </p>
      <p class="mb-4">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
      <div class="d-flex align-items-center">
       <div class="user-img" style="background-image: url(images/person_2.jpg)"></div>
       <div class="pl-3">
        <p class="name">Roger Scott</p>
        <span class="position">Marketing Manager</span>
      </div>
    </div>
  </div>
</div>
</div>
<div class="item">
  <div class="testimony-wrap py-4">
    <div class="text">
     <p class="star">
      <span class="fa fa-star"></span>
      <span class="fa fa-star"></span>
      <span class="fa fa-star"></span>
      <span class="fa fa-star"></span>
      <span class="fa fa-star"></span>
    </p>
    <p class="mb-4">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
    <div class="d-flex align-items-center">
     <div class="user-img" style="background-image: url(images/person_3.jpg)"></div>
     <div class="pl-3">
      <p class="name">Roger Scott</p>
      <span class="position">Marketing Manager</span>
    </div>
  </div>
</div>
</div>
</div>
<div class="item">
  <div class="testimony-wrap py-4">
    <div class="text">
     <p class="star">
      <span class="fa fa-star"></span>
      <span class="fa fa-star"></span>
      <span class="fa fa-star"></span>
      <span class="fa fa-star"></span>
      <span class="fa fa-star"></span>
    </p>
    <p class="mb-4">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
    <div class="d-flex align-items-center">
     <div class="user-img" style="background-image: url(images/person_1.jpg)"></div>
     <div class="pl-3">
      <p class="name">Roger Scott</p>
      <span class="position">Marketing Manager</span>
    </div>
  </div>
</div>
</div>
</div>
<div class="item">
  <div class="testimony-wrap py-4">
    <div class="text">
     <p class="star">
      <span class="fa fa-star"></span>
      <span class="fa fa-star"></span>
      <span class="fa fa-star"></span>
      <span class="fa fa-star"></span>
      <span class="fa fa-star"></span>
    </p>
    <p class="mb-4">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
    <div class="d-flex align-items-center">
     <div class="user-img" style="background-image: url(images/person_2.jpg)"></div>
     <div class="pl-3">
      <p class="name">Roger Scott</p>
      <span class="position">Marketing Manager</span>
    </div>
  </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
  <br />
<section class="ftco-intro ftco-section ftco-no-pt">
 <div class="container">
  <div class="row justify-content-center">
   <div class="col-md-12 text-center">
    <div class="img"  style="background-image: url(images/bg_2.jpg);">
     <div class="overlay"></div>
     <h2>We Are Pacific A Travel Agency</h2>
     <p>We can manage your dream building A small river named Duden flows by their place</p>
     <p class="mb-0"><a href="#" class="btn btn-primary px-4 py-3">Ask For A Quote</a></p>
   </div>
 </div>
</div>
</div>
</section>
</asp:Content>
