﻿<%@ Page Title="Hoteles" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="Hoteles.aspx.cs" Inherits="Hoteles" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
<section class="hero-wrap hero-wrap-2 js-fullheight" style="background-image: url('https://cf.bstatic.com/xdata/images/hotel/max1024x768/361696295.jpg?k=20cfb4f89cf7c25b740c4b4cceaec53456dc8129cc73ba2ac333b67ca1474d77&o=&hp=1');">
  <div class="overlay"></div>
  <div class="container">
    <div class="row no-gutters slider-text js-fullheight align-items-end justify-content-center">
      <div class="col-md-9 ftco-animate pb-5 text-center">
       <p class="breadcrumbs"><span class="mr-2"><a runat="server" href="~/">Home <i class="fa fa-chevron-right"></i></a></span> <span>Hotel <i class="fa fa-chevron-right"></i></span></p>
       <h1 class="mb-0 bread">Hotel</h1>
     </div>
   </div>
 </div>
</section>

    <div class="container mt-4">
        <br />
        <h4>Hotel List</h4>
        <div class="row mt-3">
            <div class="col-md-12">
                <button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#addHotelModal">
                    Add New Hotel
                </button>
            </div>
        </div>
        <br />
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <asp:GridView ID="gvHotels" runat="server" CssClass="table table-striped table-bordered" DataKeyNames="HotelID" OnRowCommand="gvHotels_RowCommand" OnRowEditing="gvHotels_RowEditing" OnRowDeleting="gvHotels_RowDeleting">

                    </asp:GridView>
                </div>
                <asp:HiddenField ID="hfEditHotelID" runat="server" />
                <asp:HiddenField ID="hfDeleteHotelID" runat="server" />
            </div>
        </div>
    </div>

    <!-- Add Hotel Modal -->
<div class="modal fade" id="addHotelModal" tabindex="-1" role="dialog" aria-labelledby="addHotelModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="addHotelModalLabel">Add New Hotel</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="txtNombre">Nombre:</label>
                    <asp:TextBox ID="txtNombre" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="form-group">
                    <label for="txtDireccion">Dirección:</label>
                    <asp:TextBox ID="txtDireccion" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="form-group">
                    <label for="txtPrecioPorNoche">Precio por Noche:</label>
                    <asp:TextBox ID="txtPrecioPorNoche" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="form-group">
                    <label for="txtImagenURL">Imagen URL:</label>
                    <asp:TextBox ID="txtImagenURL" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="form-group">
                    <label for="ddlDestino">Destino:</label>
                    <asp:DropDownList ID="ddlDestino" runat="server" CssClass="form-control">
                    </asp:DropDownList>
                </div>

                <!-- Label for showing error message -->
                <asp:Label ID="lblError" runat="server" CssClass="text-danger"></asp:Label>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <asp:Button ID="btnSaveHotel" runat="server" Text="Save" CssClass="btn btn-primary" OnClick="btnSaveHotel_Click" />
            </div>
        </div>
    </div>
</div>

<!-- Rest of the code -->

    <!-- Edit Hotel Modal -->
    <div class="modal fade" id="editHotelModal" tabindex="-1" role="dialog" aria-labelledby="editHotelModalLabel" aria-hidden="true">
        <!-- Modal content for editing a hotel goes here -->
    </div>

    <!-- Delete Hotel Modal -->
    <div class="modal fade" id="deleteHotelModal" tabindex="-1" role="dialog" aria-labelledby="deleteHotelModalLabel" aria-hidden="true">
        <!-- Modal content for deleting a hotel goes here -->
    </div>

    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
</asp:Content>
