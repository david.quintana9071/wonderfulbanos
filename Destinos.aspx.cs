﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using System.Web.UI;

public partial class Destinos : System.Web.UI.Page
{
    private string connectionString = ConfigurationManager.ConnectionStrings["HotelDBConnectionString"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindDestinos();
        }
    }

    private void BindDestinos()
    {
        using (SqlConnection con = new SqlConnection(connectionString))
        {
            using (SqlCommand cmd = new SqlCommand("sp_ObtenerDestinos", con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                gvDestinos.DataSource = rdr;
                gvDestinos.DataBind();
            }
        }
    }

    protected void btnSaveDestino_Click(object sender, EventArgs e)
    {
        string nombre = txtNombre.Text.Trim();
        string descripcion = txtDescripcion.Text.Trim();
        decimal precio = Convert.ToDecimal(txtPrecio.Text);
        string imagenURL = txtImagenURL.Text.Trim();

        using (SqlConnection con = new SqlConnection(connectionString))
        {
            using (SqlCommand cmd = new SqlCommand("sp_InsertarDestino", con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Nombre", nombre);
                cmd.Parameters.AddWithValue("@Descripcion", descripcion);
                cmd.Parameters.AddWithValue("@Precio", precio);
                cmd.Parameters.AddWithValue("@ImagenURL", imagenURL);
                con.Open();
                cmd.ExecuteNonQuery();
            }
        }

        ClearForm();
        BindDestinos();
    }


    // Resto del código para manejar los eventos de edición, eliminación y actualización.
    // Puedes utilizar un enfoque similar al código que se proporcionó anteriormente para Hoteles.aspx.cs.
    protected void gvDestinos_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Edit")
        {
            int rowIndex = ((GridViewRow)((LinkButton)e.CommandSource).NamingContainer).RowIndex;
            hfEditDestinoID.Value = gvDestinos.DataKeys[rowIndex].Value.ToString();
            // Aquí puedes cargar los datos del destino seleccionado en el modal de edición.
            ScriptManager.RegisterStartupScript(this, this.GetType(), "editModalScript", "$('#editDestinoModal').modal('show');", true);
        }
        else if (e.CommandName == "Delete")
        {
            int rowIndex = ((GridViewRow)((LinkButton)e.CommandSource).NamingContainer).RowIndex;
            hfDeleteDestinoID.Value = gvDestinos.DataKeys[rowIndex].Value.ToString();
            // Aquí puedes mostrar el modal de confirmación de eliminación.
            ScriptManager.RegisterStartupScript(this, this.GetType(), "deleteModalScript", "$('#deleteDestinoModal').modal('show');", true);
        }
    }

    protected void gvDestinos_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvDestinos.EditIndex = e.NewEditIndex;
        BindDestinos();
    }

    protected void gvDestinos_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int destinoID = Convert.ToInt32(gvDestinos.DataKeys[e.RowIndex].Value);

        using (SqlConnection con = new SqlConnection(connectionString))
        {
            using (SqlCommand cmd = new SqlCommand("sp_EliminarDestino", con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DestinoID", destinoID);
                con.Open();
                cmd.ExecuteNonQuery();
            }
        }

        BindDestinos();
    }

    // Agrega este método para limpiar el formulario después de guardar un destino o cancelar la edición.
    private void ClearForm()
    {
        txtNombre.Text = string.Empty;
        txtDescripcion.Text = string.Empty;
        txtPrecio.Text = string.Empty;
        txtImagenURL.Text = string.Empty;
        hfEditDestinoID.Value = string.Empty;
    }

}
