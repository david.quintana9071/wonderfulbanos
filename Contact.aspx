﻿<%@ Page Title="Contactanos" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="Contact.aspx.cs" Inherits="Contact" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <center><h2><%: Title %>.</h2></center>
   
    <!-- Dirección y detalles de contacto -->
    <!-- ... Tu código HTML existente ... -->

    <div class="container mt-4">
        <h4>Lista de Contactos</h4>
        <div class="row mt-3">
            <div class="col-md-12">
                <button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#addContactModal">
                    Agregar Nuevo Contacto
                </button>
            </div>
        </div>
        <br />
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                        <asp:GridView ID="gvContacts" runat="server" CssClass="table table-striped table-bordered" DataKeyNames="Id" OnRowCommand="gvContacts_RowCommand"  OnRowEditing="gvContacts_RowEditing" OnRowDeleting="gvContacts_RowDeleting">
                            <Columns>
                                <asp:TemplateField HeaderText="Acciones">
                                    <ItemTemplate>
                                        <div class="d-flex justify-content-between">
                                            <asp:LinkButton ID="btnEdit" runat="server" Text="Editar" CssClass="btn btn-primary" CommandName="Edit" CommandArgument='<%# Eval("Id") %>' />
                                            <asp:LinkButton ID="btnDelete" runat="server" Text="Eliminar" CssClass="btn btn-danger" CommandName="Delete" CommandArgument='<%# Eval("Id") %>' />
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
   
                        </asp:GridView>
                </div>
                <asp:HiddenField ID="hfEditContactId" runat="server" />
                <asp:HiddenField ID="hfDeleteContactId" runat="server" />
            </div>
        </div>
    </div>

    <!-- Modal Agregar Contacto -->
    <div class="modal fade" id="addContactModal" tabindex="-1" role="dialog" aria-labelledby="addContactModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="addContactModalLabel">Agregar Nuevo Contacto</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="txtName">Nombre:</label>
                        <asp:TextBox ID="txtName" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label for="txtEmail">Email:</label>
                        <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label for="txtPhoneNumber">Número de Teléfono:</label>
                        <asp:TextBox ID="txtPhoneNumber" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <asp:Button ID="btnSaveContact" runat="server" Text="Guardar Contacto" OnClick="btnSaveContact_Click" CssClass="btn btn-primary" />
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Editar Contacto -->
    <div class="modal fade" id="editContactModal" tabindex="-1" role="dialog" aria-labelledby="editContactModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="editContactModalLabel">Editar Contacto</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="txtEditName">Nombre:</label>
                        <asp:TextBox ID="txtEditName" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label for="txtEditEmail">Email:</label>
                        <asp:TextBox ID="txtEditEmail" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label for="txtEditPhoneNumber">Número de Teléfono:</label>
                        <asp:TextBox ID="txtEditPhoneNumber" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <asp:Button ID="btnUpdateContact" runat="server" Text="Actualizar Contacto" OnClick="btnUpdateContact_Click" CssClass="btn btn-primary" />
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Eliminar Contacto -->
    <div class="modal fade" id="deleteContactModal" tabindex="-1" role="dialog" aria-labelledby="deleteContactModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="deleteContactModalLabel">Confirmar Eliminación</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    ¿Estás seguro de que quieres eliminar este contacto?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                    <asp:Button ID="btnConfirmDelete" runat="server" Text="Sí" OnClick="btnConfirmDelete_Click" CssClass="btn btn-danger" />
                </div>
            </div>
        </div>
    </div>
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
</asp:Content>
