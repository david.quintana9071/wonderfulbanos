﻿<%@ Page Title="Reservar" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="Reservar.aspx.cs" Inherits="Reservar" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <section class="hero-wrap hero-wrap-2 js-fullheight" style="background-image: url('https://www.bucayadventures.com/wp-content/uploads/2018/01/DSC_0263.jpg');">
        <div class="overlay"></div>
        <div class="container">
            <div class="row no-gutters slider-text js-fullheight align-items-end justify-content-center">
                <div class="col-md-9 ftco-animate pb-5 text-center">
                    <p class="breadcrumbs">
                        <span class="mr-2">
                            <a runat="server" href="~/">Home <i class="fa fa-chevron-right"></i></a>
                        </span>
                        <span>Reservar <i class="fa fa-chevron-right"></i></span>
                    </p>
                    <h1 class="mb-0 bread">Reservar</h1>
                </div>
            </div>
        </div>
    </section>

    <div class="container mt-4">
        <br />
        <h4>Reserva List</h4>
        <div class="row mt-3">
            <div class="col-md-12">
                <button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#addReservaModal">
                    Add New Reserva
                </button>
            </div>
        </div>
        <br />
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <asp:GridView ID="gvReservas" runat="server" CssClass="table table-striped table-bordered" DataKeyNames="ReservaID"
                        OnRowCommand="gvReservas_RowCommand" OnRowEditing="gvReservas_RowEditing" OnRowDeleting="gvReservas_RowDeleting">
                    </asp:GridView>
                </div>
                <asp:HiddenField ID="hfEditReservaID" runat="server" />
                <asp:HiddenField ID="hfDeleteReservaID" runat="server" />
            </div>
        </div>
    </div>

    <!-- Add Reserva Modal -->
    <div class="modal fade" id="addReservaModal" tabindex="-1" role="dialog" aria-labelledby="addReservaModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="addReservaModalLabel">Add New Reserva</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="txtFechaInicio">Fecha de Inicio:</label>
                        <asp:TextBox ID="txtFechaInicio" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label for="txtFechaFin">Fecha de Fin:</label>
                        <asp:TextBox ID="txtFechaFin" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label for="txtCantidadPersonas">Cantidad de Personas:</label>
                        <asp:TextBox ID="txtCantidadPersonas" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label for="txtTotalPago">Total Pago:</label>
                        <asp:TextBox ID="txtTotalPago" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label for="ddlCliente">Cliente:</label>
                        <asp:DropDownList ID="ddlCliente" runat="server" CssClass="form-control">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group">
                        <label for="ddlHotel">Hotel:</label>
                        <asp:DropDownList ID="ddlHotel" runat="server" CssClass="form-control">
                        </asp:DropDownList>
                    </div>

                    <!-- Label for showing error message -->
                    <asp:Label ID="lblError" runat="server" CssClass="text-danger"></asp:Label>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <asp:Button ID="btnSaveReserva" runat="server" Text="Save" CssClass="btn btn-primary" OnClick="btnSaveReserva_Click" />
                </div>
            </div>
        </div>
    </div>

    <!-- Rest of the code -->

    <!-- Edit Reserva Modal -->
    <div class="modal fade" id="editReservaModal" tabindex="-1" role="dialog" aria-labelledby="editReservaModalLabel"
        aria-hidden="true">
        <!-- Modal content for editing a reserva goes here -->
    </div>

    <!-- Delete Reserva Modal -->
    <div class="modal fade" id="deleteReservaModal" tabindex="-1" role="dialog" aria-labelledby="deleteReservaModalLabel"
        aria-hidden="true">
        <!-- Modal content for deleting a reserva goes here -->
    </div>

    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
</asp:Content>
