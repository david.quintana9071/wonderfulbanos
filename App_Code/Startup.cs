﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Webaños.Startup))]
namespace Webaños
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
