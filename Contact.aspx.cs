﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using System.Web.UI;

public partial class Contact : System.Web.UI.Page
{
    // Define la cadena de conexión a la base de datos
    private string connectionString = ConfigurationManager.ConnectionStrings["HotelDBConnectionString"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            // Carga los contactos al GridView
            BindContacts();
        }
    }

    // Método para cargar los contactos al GridView
    private void BindContacts()
    {
        using (SqlConnection con = new SqlConnection(connectionString))
        {
            using (SqlCommand cmd = new SqlCommand("sp_GetAllContacts", con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                gvContacts.DataSource = rdr;
                gvContacts.DataBind();
            }
        }
    }

    // Evento del botón "Guardar" para agregar un nuevo contacto
    protected void btnSaveContact_Click(object sender, EventArgs e)
    {
        string name = txtName.Text.Trim();
        string email = txtEmail.Text.Trim();
        string phoneNumber = txtPhoneNumber.Text.Trim();

        using (SqlConnection con = new SqlConnection(connectionString))
        {
            using (SqlCommand cmd = new SqlCommand("sp_InsertContact", con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Name", name);
                cmd.Parameters.AddWithValue("@Email", email);
                cmd.Parameters.AddWithValue("@PhoneNumber", phoneNumber);
                con.Open();
                cmd.ExecuteNonQuery();
            }
        }

        // Limpia el formulario después de guardar el contacto
        ClearForm();

        // Actualiza la lista de contactos en el GridView
        BindContacts();
    }

    // ... Resto del código ...

    // Método para limpiar el formulario de agregar nuevo contacto
    private void ClearForm()
    {
        txtName.Text = "";
        txtEmail.Text = "";
        txtPhoneNumber.Text = "";
    }

    // Evento del botón "Editar" para cargar los detalles del contacto en el modal de edición
    protected void gvContacts_RowEditing(object sender, GridViewEditEventArgs e)
    {
        int contactId = Convert.ToInt32(gvContacts.DataKeys[e.NewEditIndex].Value);
        LoadContactDetailsForEdit(contactId);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "editModalScript", "$('#editContactModal').modal('show');", true);
    }

    // Evento del botón "Actualizar" para actualizar un contacto existente
    protected void btnUpdateContact_Click(object sender, EventArgs e)
    {
        int contactId = Convert.ToInt32(hfEditContactId.Value);
        string name = txtEditName.Text.Trim();
        string email = txtEditEmail.Text.Trim();
        string phoneNumber = txtEditPhoneNumber.Text.Trim();

        using (SqlConnection con = new SqlConnection(connectionString))
        {
            using (SqlCommand cmd = new SqlCommand("sp_UpdateContact", con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Id", contactId);
                cmd.Parameters.AddWithValue("@Name", name);
                cmd.Parameters.AddWithValue("@Email", email);
                cmd.Parameters.AddWithValue("@PhoneNumber", phoneNumber);
                con.Open();
                cmd.ExecuteNonQuery();
            }
        }

        // Cierra el modal después de actualizar el contacto
        ScriptManager.RegisterStartupScript(this, this.GetType(), "editModalScript", "$('#editContactModal').modal('hide');", true);

        BindContacts();
    }

    protected void gvContacts_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        if (e.RowIndex >= 0 && e.RowIndex < gvContacts.Rows.Count)
        {
            int contactId = Convert.ToInt32(gvContacts.DataKeys[e.RowIndex].Value);

            // Guarda el Id del contacto a eliminar en un campo oculto para usarlo después en el evento de confirmación
            hfDeleteContactId.Value = contactId.ToString();

            // Muestra el modal de confirmación de eliminación
            ScriptManager.RegisterStartupScript(this, this.GetType(), "deleteModalScript", "$('#deleteContactModal').modal('show');", true);
        }

        // Cancela la eliminación automática del GridView
        gvContacts.EditIndex = -1; // Asegurarse de que no haya una fila en modo de edición
        e.Cancel = true;
    }


    // Evento del botón "Confirmar Eliminación" en el modal de eliminación
    protected void btnConfirmDelete_Click(object sender, EventArgs e)
    {
        int contactId = Convert.ToInt32(hfDeleteContactId.Value);

        using (SqlConnection con = new SqlConnection(connectionString))
        {
            using (SqlCommand cmd = new SqlCommand("sp_DeleteContact", con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Id", contactId);
                con.Open();
                cmd.ExecuteNonQuery();
            }
        }

        // Cierra el modal después de eliminar el contacto
        ScriptManager.RegisterStartupScript(this, this.GetType(), "deleteModalScript", "$('#deleteContactModal').modal('hide');", true);

        // Actualiza la lista de contactos en el GridView
        BindContacts();
    }



    // ... Resto del código ...

    // Método para cargar los detalles del contacto en el modal de edición
    private void LoadContactDetailsForEdit(int contactId)
    {
        using (SqlConnection con = new SqlConnection(connectionString))
        {
            string query = "SELECT Name, Email, PhoneNumber FROM Contacts WHERE Id = @ContactId";
            using (SqlCommand cmd = new SqlCommand(query, con))
            {
                cmd.Parameters.AddWithValue("@ContactId", contactId);
                con.Open();
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        txtEditName.Text = reader["Name"].ToString();
                        txtEditEmail.Text = reader["Email"].ToString();
                        txtEditPhoneNumber.Text = reader["PhoneNumber"].ToString();
                        hfEditContactId.Value = contactId.ToString(); // Guarda el Id del contacto en un campo oculto para usarlo en la actualización
                    }
                }
            }
        }
    }
    protected void gvContacts_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Delete")
        {
            int contactId = Convert.ToInt32(e.CommandArgument);
            DeleteContact(contactId);
            BindContacts();
        }
    }
    private void DeleteContact(int contactId)
    {
        using (SqlConnection con = new SqlConnection(connectionString))
        {
            using (SqlCommand cmd = new SqlCommand("sp_DeleteContact", con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Id", contactId);
                con.Open();
                cmd.ExecuteNonQuery();
            }
        }
    }


}
