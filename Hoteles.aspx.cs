﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using System.Web.UI;

public partial class Hoteles : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            // Bind the DropDownList with real destinations from the database
            BindDestinationsDropDownList();

            // Bind the GridView with hotels data
            BindHotelsGrid();
        }
    }
    private void BindDestinationsDropDownList()
    {
        string connectionString = ConfigurationManager.ConnectionStrings["HotelDBConnectionString"].ConnectionString;
        string query = "SELECT DestinoID, Nombre FROM Destinos"; // Replace "Destinos" with your actual table name

        using (SqlConnection connection = new SqlConnection(connectionString))
        {
            using (SqlCommand command = new SqlCommand(query, connection))
            {
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();

                // Clear existing items in the DropDownList
                ddlDestino.Items.Clear();

                // Add a default item to show as the initial option (optional)
                ddlDestino.Items.Add(new ListItem("-- Select Destination --", "-1"));

                while (reader.Read())
                {
                    int destinoID = Convert.ToInt32(reader["DestinoID"]);
                    string nombre = reader["Nombre"].ToString();

                    // Add the destination name as an item to the DropDownList with destinoID as the value
                    ddlDestino.Items.Add(new ListItem(nombre, destinoID.ToString()));
                }

                reader.Close();
            }
        }
    }


    protected void BindHotelsGrid()
    {
        string connectionString = ConfigurationManager.ConnectionStrings["HotelDBConnectionString"].ConnectionString;
        string query = "SELECT H.HotelID, H.Nombre AS HotelNombre, H.Direccion, H.PrecioPorNoche, H.ImagenURL, D.Nombre AS DestinoNombre FROM Hoteles H INNER JOIN Destinos D ON H.DestinoID = D.DestinoID";

        using (SqlConnection connection = new SqlConnection(connectionString))
        {
            using (SqlCommand command = new SqlCommand(query, connection))
            {
                connection.Open();
                SqlDataAdapter adapter = new SqlDataAdapter(command);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                gvHotels.DataSource = dt;
                gvHotels.DataBind();
            }
        }
    }



    protected void gvHotels_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Edit")
        {
            int rowIndex = ((GridViewRow)((LinkButton)e.CommandSource).NamingContainer).RowIndex;
            hfEditHotelID.Value = gvHotels.DataKeys[rowIndex].Value.ToString();
            // Retrieve the hotel details from the database and populate the edit modal.
            // Modal content for editing a hotel goes here
        }
        else if (e.CommandName == "Delete")
        {
            int rowIndex = ((GridViewRow)((LinkButton)e.CommandSource).NamingContainer).RowIndex;
            hfDeleteHotelID.Value = gvHotels.DataKeys[rowIndex].Value.ToString();
            // Show the delete confirmation modal.
            // Modal content for deleting a hotel goes here
        }
    }

    protected void gvHotels_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvHotels.EditIndex = e.NewEditIndex;
        BindHotelsGrid();
    }

    protected void gvHotels_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int hotelID = Convert.ToInt32(gvHotels.DataKeys[e.RowIndex].Value);
        DeleteHotel(hotelID);
    }

    private void DeleteHotel(int hotelID)
    {
        string connectionString = ConfigurationManager.ConnectionStrings["HotelDBConnectionString"].ConnectionString;
        string query = "EXEC sp_EliminarHotel @HotelID";

        using (SqlConnection connection = new SqlConnection(connectionString))
        {
            using (SqlCommand command = new SqlCommand(query, connection))
            {
                command.Parameters.AddWithValue("@HotelID", hotelID);
                connection.Open();
                command.ExecuteNonQuery();
            }
        }

        BindHotelsGrid();
    }

    protected void btnSaveHotel_Click(object sender, EventArgs e)
    {
        string connectionString = ConfigurationManager.ConnectionStrings["HotelDBConnectionString"].ConnectionString;
        string query = "EXEC sp_InsertarHotel @Nombre, @Direccion, @PrecioPorNoche, @ImagenURL, @DestinoID";

        using (SqlConnection connection = new SqlConnection(connectionString))
        {
            using (SqlCommand command = new SqlCommand(query, connection))
            {
                // Verificar si el valor del precio por noche es numérico y válido
                decimal precioPorNoche;
                if (decimal.TryParse(txtPrecioPorNoche.Text, out precioPorNoche))
                {
                    // El valor es numérico y se puede utilizar para la inserción
                    command.Parameters.AddWithValue("@PrecioPorNoche", precioPorNoche);
                }
                else
                {
                    // El valor no es un número decimal válido
                    // Puedes mostrar un mensaje de error o tomar alguna otra acción en este caso
                    // Por ejemplo, mostrar un mensaje de error y detener la inserción
                    lblError.Text = "Precio por noche debe ser un valor numérico válido.";
                    return;
                }

                // Agregar los demás parámetros del procedimiento almacenado
                command.Parameters.AddWithValue("@Nombre", txtNombre.Text);
                command.Parameters.AddWithValue("@Direccion", txtDireccion.Text);
                command.Parameters.AddWithValue("@ImagenURL", txtImagenURL.Text);
                command.Parameters.AddWithValue("@DestinoID", Convert.ToInt32(ddlDestino.SelectedValue));

                connection.Open();
                command.ExecuteNonQuery();
            }
        }

        ClearForm();
        BindHotelsGrid();
    }


    private void ClearForm()
    {
        txtNombre.Text = "";
        txtDireccion.Text = "";
        txtPrecioPorNoche.Text = "";
        txtImagenURL.Text = "";
        ddlDestino.SelectedIndex = 0;
    }
}
