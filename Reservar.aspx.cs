﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using System.Web.UI;

public partial class Reservar : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            // Bind the DropDownList with real clients from the database
            BindClientesDropDownList();

            // Bind the DropDownList with real hotels from the database
            BindHotelesDropDownList();

            // Bind the GridView with reservations data
            BindReservasGrid();
        }
    }

    private void BindClientesDropDownList()
    {
        string connectionString = ConfigurationManager.ConnectionStrings["HotelDBConnectionString"].ConnectionString;
        string query = "SELECT ClienteID, Nombre FROM Cliente"; // Replace "Clientes" with your actual table name

        using (SqlConnection connection = new SqlConnection(connectionString))
        {
            using (SqlCommand command = new SqlCommand(query, connection))
            {
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();

                // Clear existing items in the DropDownList
                ddlCliente.Items.Clear();

                // Add a default item to show as the initial option (optional)
                ddlCliente.Items.Add(new ListItem("-- Select Cliente --", "-1"));

                while (reader.Read())
                {
                    int clienteID = Convert.ToInt32(reader["ClienteID"]);
                    string nombre = reader["Nombre"].ToString();

                    // Add the client name as an item to the DropDownList with clienteID as the value
                    ddlCliente.Items.Add(new ListItem(nombre, clienteID.ToString()));
                }

                reader.Close();
            }
        }
    }

    private void BindHotelesDropDownList()
    {
        string connectionString = ConfigurationManager.ConnectionStrings["HotelDBConnectionString"].ConnectionString;
        string query = "SELECT HotelID, Nombre FROM Hoteles"; // Replace "Hoteles" with your actual table name

        using (SqlConnection connection = new SqlConnection(connectionString))
        {
            using (SqlCommand command = new SqlCommand(query, connection))
            {
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();

                // Clear existing items in the DropDownList
                ddlHotel.Items.Clear();

                // Add a default item to show as the initial option (optional)
                ddlHotel.Items.Add(new ListItem("-- Select Hotel --", "-1"));

                while (reader.Read())
                {
                    int hotelID = Convert.ToInt32(reader["HotelID"]);
                    string nombre = reader["Nombre"].ToString();

                    // Add the hotel name as an item to the DropDownList with hotelID as the value
                    ddlHotel.Items.Add(new ListItem(nombre, hotelID.ToString()));
                }

                reader.Close();
            }
        }
    }

    protected void BindReservasGrid()
    {
        string connectionString = ConfigurationManager.ConnectionStrings["HotelDBConnectionString"].ConnectionString;
        string query = "SELECT ReservaID, FechaInicio, FechaFin, CantidadPersonas, TotalPago, ClienteID, HotelID FROM Reservas"; // Replace "Reservas" with your actual table name

        using (SqlConnection connection = new SqlConnection(connectionString))
        {
            using (SqlCommand command = new SqlCommand(query, connection))
            {
                connection.Open();
                SqlDataAdapter adapter = new SqlDataAdapter(command);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                gvReservas.DataSource = dt;
                gvReservas.DataBind();
            }
        }
    }

    protected void gvReservas_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Edit")
        {
            int rowIndex = ((GridViewRow)((LinkButton)e.CommandSource).NamingContainer).RowIndex;
            hfEditReservaID.Value = gvReservas.DataKeys[rowIndex].Value.ToString();
            // Retrieve the reservation details from the database and populate the edit modal.
            // Modal content for editing a reservation goes here
        }
        else if (e.CommandName == "Delete")
        {
            int rowIndex = ((GridViewRow)((LinkButton)e.CommandSource).NamingContainer).RowIndex;
            hfDeleteReservaID.Value = gvReservas.DataKeys[rowIndex].Value.ToString();
            // Show the delete confirmation modal.
            // Modal content for deleting a reservation goes here
        }
    }

    protected void gvReservas_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvReservas.EditIndex = e.NewEditIndex;
        BindReservasGrid();
    }

    protected void gvReservas_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int reservaID = Convert.ToInt32(gvReservas.DataKeys[e.RowIndex].Value);
        DeleteReserva(reservaID);
    }

    private void DeleteReserva(int reservaID)
    {
        string connectionString = ConfigurationManager.ConnectionStrings["HotelDBConnectionString"].ConnectionString;
        string query = "EXEC sp_EliminarReserva @ReservaID";

        using (SqlConnection connection = new SqlConnection(connectionString))
        {
            using (SqlCommand command = new SqlCommand(query, connection))
            {
                command.Parameters.AddWithValue("@ReservaID", reservaID);
                connection.Open();
                command.ExecuteNonQuery();
            }
        }

        BindReservasGrid();
    }

    protected void btnSaveReserva_Click(object sender, EventArgs e)
    {
        string connectionString = ConfigurationManager.ConnectionStrings["HotelDBConnectionString"].ConnectionString;
        string query = "EXEC sp_InsertarReserva @FechaInicio, @FechaFin, @CantidadPersonas, @TotalPago, @ClienteID, @HotelID";

        using (SqlConnection connection = new SqlConnection(connectionString))
        {
            using (SqlCommand command = new SqlCommand(query, connection))
            {
                // Verificar si el valor del total de pago es numérico y válido
                decimal totalPago;
                if (decimal.TryParse(txtTotalPago.Text, out totalPago))
                {
                    // El valor es numérico y se puede utilizar para la inserción
                    command.Parameters.AddWithValue("@TotalPago", totalPago);
                }
                else
                {
                    // El valor no es un número decimal válido
                    // Puedes mostrar un mensaje de error o tomar alguna otra acción en este caso
                    // Por ejemplo, mostrar un mensaje de error y detener la inserción
                    lblError.Text = "Total Pago debe ser un valor numérico válido.";
                    return;
                }

                // Agregar los demás parámetros del procedimiento almacenado
                command.Parameters.AddWithValue("@FechaInicio", Convert.ToDateTime(txtFechaInicio.Text));
                command.Parameters.AddWithValue("@FechaFin", Convert.ToDateTime(txtFechaFin.Text));
                command.Parameters.AddWithValue("@CantidadPersonas", Convert.ToInt32(txtCantidadPersonas.Text));
                command.Parameters.AddWithValue("@ClienteID", Convert.ToInt32(ddlCliente.SelectedValue));
                command.Parameters.AddWithValue("@HotelID", Convert.ToInt32(ddlHotel.SelectedValue));

                connection.Open();
                command.ExecuteNonQuery();
            }
        }

        ClearForm();
        BindReservasGrid();
    }

    private void ClearForm()
    {
        txtFechaInicio.Text = "";
        txtFechaFin.Text = "";
        txtCantidadPersonas.Text = "";
        txtTotalPago.Text = "";
        ddlCliente.SelectedIndex = 0;
        ddlHotel.SelectedIndex = 0;
    }
}
