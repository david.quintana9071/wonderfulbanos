﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.WebControls;

public partial class Cliente : System.Web.UI.Page
{
    private string connectionString = ConfigurationManager.ConnectionStrings["HotelDBConnectionString"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindClients();
        }
    }

    private void BindClients()
    {
        using (SqlConnection con = new SqlConnection(connectionString))
        {
            using (SqlCommand cmd = new SqlCommand("sp_ObtenerClientes", con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                gvClients.DataSource = rdr;
                gvClients.DataBind();
            }
        }
    }

    protected void btnSaveClient_Click(object sender, EventArgs e)
    {
        string name = txtName.Text.Trim();
        string apellido = txtApellido.Text.Trim();
        string email = txtEmail.Text.Trim();
        string phoneNumber = txtPhoneNumber.Text.Trim();
        string direccion = txtDireccion.Text.Trim();

        using (SqlConnection con = new SqlConnection(connectionString))
        {
            using (SqlCommand cmd = new SqlCommand("sp_InsertarCliente", con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Nombre", name);
                cmd.Parameters.AddWithValue("@Apellido", apellido);
                cmd.Parameters.AddWithValue("@Correo", email);
                cmd.Parameters.AddWithValue("@Telefono", phoneNumber);
                cmd.Parameters.AddWithValue("@Direccion", direccion);
                con.Open();
                cmd.ExecuteNonQuery();
            }
        }

        ClearForm();
        BindClients();
    }

    private void ClearForm()
    {
        txtName.Text = "";
        txtApellido.Text = "";
        txtEmail.Text = "";
        txtPhoneNumber.Text = "";
        txtDireccion.Text = "";
    }
    protected void gvClients_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Edit")
        {
            int clientID = Convert.ToInt32(e.CommandArgument);
            // Aquí puedes abrir el modal de edición con los detalles del cliente según su ID
            // Llamada a la función que cargue los detalles del cliente para editar en el modal
        }
        else if (e.CommandName == "Delete")
        {
            int clientID = Convert.ToInt32(e.CommandArgument);
            // Aquí puedes abrir el modal de confirmación de eliminación y almacenar el ID del cliente a eliminar
            // Llamada a la función que muestre el modal de confirmación de eliminación
        }
    }

    protected void gvClients_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        // Cancela la eliminación automática del GridView
        gvClients.EditIndex = -1; // Asegurarse de que no haya una fila en modo de edición
        e.Cancel = true;
    }

    protected void gvClients_RowEditing(object sender, GridViewEditEventArgs e)
    {
        int clientID = Convert.ToInt32(gvClients.DataKeys[e.NewEditIndex].Value);
        // Aquí puedes cargar los detalles del cliente para editar en el modal según su ID
        // Llamada a la función que cargue los detalles del cliente para editar en el modal
    }

}
